import { useState, useCallback, useRef, useEffect, useContext } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { GetServerSideProps, NextPage } from 'next';
import { Carousel } from 'react-responsive-carousel';
import {
  faChevronDown,
  faCircle,
  faSpinner,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import FillContainImage from '../../components/FillContainImage';
import HeadingLinks from '../../components/HeadingLinks';
import ReviewProductCarousel from '../../components/ReviewProductCarousel';
import Button from '../../components/Button';
import clsx from 'clsx';
import SizeTable, { IItemSize, SizeUnit } from '../../components/SizeTable';
import ImageViewer from '../../components/ImageViewer';
import RecommendProductSlider from '../../components/RecommendProductSlider';
import Rating from '../../components/Rating';
import ReviewProgressBar from '../../components/ReviewProgressBar';
import ReviewComment from '../../components/ReviewComment';
import {
  fetchProductDetailById,
  fetchProductDetailByPath,
  fetchProductList,
} from '../../services/ProductService';
import { useFormik } from 'formik';
import { useRouter } from 'next/router';
import { ROUTES } from '../../constants/routes';
import ProductStore, { IProduct } from '../../stores/ProductStore';
import { observer } from 'mobx-react-lite';
import { getProductName } from '../../utils/string';
import UserStore from '../../stores/UserStore';
import { getRoundedDecimal } from '../../utils/math';
import { LIMIT_RECENT_PRODUCTS, LIMIT_REVIEWS } from '../../constants/limit';
import { RECENT_PRODUCTS, TOKEN } from '../../constants/storage';
import { IRecentProduct } from '../account/recent-items';
import ReviewForm from '../../components/ReviewForm';
import ReviewStore from '../../stores/ReviewStore';
import { LayoutContext } from '../../layouts/UserLayout';

type ProductTab = 'features' | 'size-guide' | 'materials' | 'care';

const PRODUCT_TABS: Array<{ title: string; value: ProductTab }> = [
  { title: 'FEATURES', value: 'features' },
  { title: 'SIZE GUIDE', value: 'size-guide' },
  { title: 'MATERIALS', value: 'materials' },
  { title: 'CARE', value: 'care' },
];

const REGION_SIZES: Array<{ region: string; startSize: number }> = [
  { region: 'UK', startSize: 6 },
  { region: 'EUR', startSize: 34 },
  { region: 'US', startSize: 2 },
];

const CENTIMETER_SIZES: IItemSize[] = [
  { title: 'BUST', sizes: [29, 31, 33, 35, 37], note: null },
  {
    title: 'LENGTH',
    sizes: [13, 14, 14, 15, 15],
    note: 'Length is measured from shoulder to hem',
  },
  {
    title: 'SLEEVES',
    sizes: [13, 13, 13, 14, 14],
    note: 'Sleeve length is measured from shoulder to cuff',
  },
];

const RATING_LEVELS: Array<{ title: string; star: number }> = [
  { title: 'Excellent', star: 5 },
  { title: 'Great', star: 4 },
  { title: 'Average', star: 3 },
  { title: 'Poor', star: 2 },
  { title: 'Bad', star: 1 },
];

interface IProps {
  productInfo: IProduct;
}

const ProductPage: NextPage<IProps> = ({ productInfo }) => {
  const [selectedTab, setSelectedTab] = useState<ProductTab>('features');
  const [sizeUnit, setSizeUnit] = useState<SizeUnit>('cm');
  const [isOpeningImageViewer, setIsOpeningImageViewer] =
    useState<boolean>(false);
  const [openingImageIndex, setOpeningImageIndex] = useState<number>(0);
  const [otherColorProducts, setOtherColorProducts] = useState<IProduct[]>([]);

  const tabContainerRef = useRef<HTMLDivElement>(null);
  const reviewHeaderRef = useRef<HTMLDivElement>(null);

  const { selectedCurrency, currencyExchangeRate } = UserStore;
  const { recommendProducts, addCartProduct, dispatchFetchRecommendProducts } =
    ProductStore;
  const {
    currentPage,
    totalPage,
    fetchingReview,
    reviews,
    reviewCounts,
    grantedReview,
    dispatchGetReviewList,
    dispatchGetReviewPermission,
    dispatchGetReviewCountList,
    clearReviewData,
  } = ReviewStore;

  const { scrollMainToTop } = useContext(LayoutContext);

  const router = useRouter();

  const { handleSubmit, handleChange, handleBlur, values, errors, touched } =
    useFormik({
      enableReinitialize: true,
      initialValues: {
        size: '',
        quantity: '1',
      },
      validate: values => {
        const errors: any = {};
        if (!values.size) {
          errors.size = 'Please choose an option for size';
        } else {
          const sizeOption = productInfo.size.find(
            size => size.name === values.size,
          );

          if (sizeOption) {
            try {
              const quantityNum = parseInt(values.quantity);
              if (sizeOption.stock < quantityNum) {
                errors.quantity = `This size only has ${sizeOption.stock} products`;
              }
            } catch {
              errors.quantity = 'Quantity must be an interger';
            }
          }
        }

        return errors;
      },
      onSubmit: values => {
        const { size, quantity } = values;

        // @ts-ignore
        addCartProduct({
          size,
          quantity: parseInt(quantity),
          productId: productInfo._id,
          parentId: productInfo.parentId,
          categoryId: productInfo.categoryId as string,
          sku: productInfo.size.find(size => size.name === values.size)
            ?.sku as string,
          path: router.asPath.slice(10),
          _id: Date.now(),
        });
        router.push(ROUTES.CART);
      },
    });

  useEffect(() => {
    if (router.pathname === ROUTES.PRODUCT) {
      scrollMainToTop();
    }
  }, [router.asPath]);

  useEffect(() => {
    if (localStorage.getItem(TOKEN)) {
      dispatchGetReviewPermission(productInfo._id);
    }
  }, [router.asPath]);

  useEffect(() => {
    if (productInfo?.categoryId) {
      dispatchFetchRecommendProducts(
        productInfo.categoryId,
        productInfo.parentId,
      );
    }
  }, [router.asPath]);

  useEffect(() => {
    const { _id, path, oldPrice, unitPrice, images } = productInfo;
    const viewedProduct = {
      _id,
      path,
      oldPrice,
      unitPrice,
      imageUrl: images.find(image => image.priority === 1)?.imageUrl || '',
    };
    const recentProductStorage = localStorage.getItem(RECENT_PRODUCTS);
    if (recentProductStorage) {
      try {
        const recentProducts: IRecentProduct[] =
          JSON.parse(recentProductStorage);
        if (!recentProducts.find(product => product._id === _id)) {
          if (recentProducts.length >= LIMIT_RECENT_PRODUCTS) {
            recentProducts.pop();
          }
          recentProducts.unshift(viewedProduct);
          localStorage.setItem(RECENT_PRODUCTS, JSON.stringify(recentProducts));
        }
      } catch (err) {
        console.error(err);
        localStorage.removeItem(RECENT_PRODUCTS);
      }
    } else {
      localStorage.setItem(RECENT_PRODUCTS, JSON.stringify([viewedProduct]));
    }
  }, [router.asPath]);

  useEffect(() => {
    (async () => {
      try {
        const result = await fetchProductList({
          parentId: productInfo.parentId,
        });

        setOtherColorProducts(
          // @ts-ignore
          result.list.filter(product => product._id !== productInfo._id),
        );
      } catch (err) {
        console.error(err);
      }
    })();
  }, [router.asPath]);

  useEffect(() => {
    clearReviewData();
    dispatchGetReviewList(productInfo._id, 1, LIMIT_REVIEWS);
    dispatchGetReviewCountList(productInfo._id);
  }, [router.asPath]);

  const handleClickPrev = () => {
    setOpeningImageIndex(
      openingImageIndex === 0
        ? productInfo.images.length - 1
        : openingImageIndex - 1,
    );
  };

  const handleClickNext = () => {
    setOpeningImageIndex(
      openingImageIndex === productInfo.images.length - 1
        ? 0
        : openingImageIndex + 1,
    );
  };

  const handleClickImage = useCallback((index: number) => {
    setIsOpeningImageViewer(true);
    setOpeningImageIndex(index);
  }, []);

  const handleScrollToReview = () => {
    reviewHeaderRef?.current &&
      reviewHeaderRef.current.scrollIntoView({ behavior: 'smooth' });
  };

  const handleScrollToSizeGuide = () => {
    if (tabContainerRef?.current) {
      setSelectedTab('size-guide');
      tabContainerRef.current.scrollIntoView({ behavior: 'smooth' });
    }
  };

  const carouselContainerCallbackRef = useCallback(
    (carouselContainer: HTMLDivElement) => {
      if (carouselContainer) {
        const slides = carouselContainer.querySelectorAll('ul.slider>li.slide');
        for (let i = 0; i < slides.length; i++) {
          const slide = slides[i] as HTMLLIElement;
          slide.onclick = () => handleClickImage(i);
        }
      }
    },
    [handleClickImage],
  );

  const totalReview = reviewCounts.reduce(
    (total, review) => total + review.count,
    0,
  );

  const averageRating = totalReview
    ? Math.round(
        (10 *
          reviewCounts.reduce(
            (total, review) => total + review.count * review._id,
            0,
          )) /
          totalReview,
      ) / 10
    : 0;

  const productImages = productInfo?.images
    .sort((a, b) => a.priority - b.priority)
    .map(image => ({
      title: `${image.imageUrl} - ${image.priority}`,
      url: image.imageUrl,
    }));

  return (
    <div>
      <HeadingLinks />
      <div className="relative pb-10">
        <div className="relative custom-product-carousel">
          <div className="hidden sm:block">
            <ReviewProductCarousel
              images={productImages}
              onClickImage={handleClickImage}
            />
          </div>
          <div
            ref={carouselContainerCallbackRef}
            className="sm:hidden bg-preview-product"
          >
            <Carousel showStatus={false} showArrows={false} showThumbs={false}>
              {productImages.map((image, index) => (
                <FillContainImage
                  key={index}
                  src={image.url}
                  alt={image.title}
                  className="w-full h-full"
                />
              ))}
            </Carousel>
          </div>
        </div>
        <div className="mt-4 sm:absolute sm:top-8 sm:right-10 sm:w-96 flex flex-col items-center">
          {!!totalReview && (
            <button onClick={handleScrollToReview} className="flex">
              <Rating rating={averageRating} size="sm" />
              <div className="ml-2 flex items-center">
                <span className="text-sm">
                  {`${totalReview} review${totalReview > 1 && 's'}`}
                </span>
                <FontAwesomeIcon
                  icon={faChevronDown}
                  className="text-xxs ml-1"
                />
              </div>
            </button>
          )}
          <form onSubmit={handleSubmit} className="bg-white rounded-md mt-4">
            <div className="flex px-6 pt-6">
              <div className="flex-auto flex flex-col">
                <h2 className="font-dosis font-extrabold text-xl uppercase">
                  {getProductName(productInfo.name, productInfo.colour)}
                </h2>
                <div className="font-dosis font-bold text-sm mt-4">SIZE:</div>
                <select
                  name="size"
                  value={values.size}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className="w-60 sm:w-64 text-sm bg-gray-100 mt-2 px-2 py-1 rounded-md"
                >
                  <option value="">
                    Please choose an option to add this product to your cart
                  </option>
                  {productInfo.size.map((size, index) => (
                    <option key={index} value={size.name}>
                      {size.name}
                    </option>
                  ))}
                </select>
                {touched.size && errors.size && (
                  <div className="text-red-500 text-sm mt-2">{errors.size}</div>
                )}
                <button
                  type="button"
                  onClick={handleScrollToSizeGuide}
                  className="w-fit px-3 py-1 mt-4 rounded-full border border-gray-500 font-dosis font-bold text-xs"
                >
                  SIZE GUIDE
                </button>
                <div className="mt-4">
                  <span>Quantity: </span>
                  <input
                    name="quantity"
                    type="number"
                    min={1}
                    value={values.quantity}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className="inline-block w-12 h-6 bg-gray-100 text-center rounded-md"
                  />
                </div>
                {touched.quantity && errors.quantity && (
                  <div className="text-red-500 text-sm mt-2">
                    {errors.quantity}
                  </div>
                )}
              </div>
              <div className="flex-none w-20 text-right">
                <div className="text-xs text-gray-700">
                  <span>RRP:&nbsp;</span>
                  <span className="line-through">
                    {`${selectedCurrency.currencyUnit}${getRoundedDecimal(
                      currencyExchangeRate * productInfo.oldPrice,
                      2,
                    )}`}
                  </span>
                </div>
                <div className="text-red-500 font-bold text-lg">
                  {`${selectedCurrency.currencyUnit}${getRoundedDecimal(
                    currencyExchangeRate * productInfo.unitPrice,
                    2,
                  )}`}
                </div>
              </div>
            </div>
            <div className="mx-4 sm:mx-0">
              <Button
                type="submit"
                title="ADD TO CART"
                block
                className="!rounded-md mt-12"
              />
            </div>
          </form>
        </div>
      </div>
      <div className="px-4 sm:px-40 font-dosis">
        <div className="text-center font-bold text-2xl">
          A BIT ABOUT THE PRODUCT
        </div>
        <h5 className="text-center">{productInfo.description}</h5>
        <h4 className="font-bold text-center py-8">
          The model is 4&apos; 6&quot; &amp; wears size 9 - 10.
        </h4>
        {!!otherColorProducts.length && (
          <>
            <div className="text-center">COLOURS AVAILABLE</div>
            <div className="flex gap-3 py-4 justify-center">
              {otherColorProducts.map(product => (
                <Link
                  key={product._id}
                  href={`${product.path}?id=${product._id}`}
                >
                  <a className="block rounded-full overflow-hidden w-[50px] h-[50px] sm:w-[100px] sm:h-[100px]">
                    <Image
                      src={
                        product.images.find(image => image.priority === 1)
                          ?.imageUrl || '/images/logo-black.png'
                      }
                      alt={product.name}
                      width="100%"
                      height="100%"
                      objectFit="cover"
                    />
                  </a>
                </Link>
              ))}
            </div>
          </>
        )}
      </div>
      <div ref={tabContainerRef} className="my-8">
        <div className="flex justify-center font-dosis">
          {PRODUCT_TABS.map((tab, index) => (
            <button
              key={index}
              onClick={() => setSelectedTab(tab.value)}
              className={clsx(
                {
                  'bg-gray-100 text-yellow-600 rounded-t-lg':
                    tab.value === selectedTab,
                },
                'px-3 py-2 font-bold sm:px-8 sm:py-3 sm:font-extrabold',
              )}
            >
              {tab.title}
            </button>
          ))}
        </div>
        <div className="bg-gray-100 px-4 lg:px-20 xl:px-40 3xl:px-80 py-8">
          <div className={clsx({ hidden: selectedTab !== 'features' })}>
            <ul className="list-circle w-fit text-sm m-auto">
              {productInfo.features.map((feature, index) => (
                <li key={index}>{feature}</li>
              ))}
            </ul>
          </div>
          <div
            className={clsx(
              { hidden: selectedTab !== 'size-guide' },
              'flex justify-end relative font-dosis h-[34rem]',
            )}
          >
            <div className="hidden sm:block w-[544px] overflow-hidden rounded-tl-[40%] rounded-bl-[97%]">
              <Image
                src="/images/sizing-guide.jpg"
                alt="Sizing guide"
                width={544}
                height={460}
              />
            </div>
            <div className="absolute inset-y-0 left-0 sm:w-3/5">
              <div className="sm:w-2/3">
                <div className="font-bold text-xl text-center sm:text-left">
                  SIZE GUIDE FOR THIS ITEM
                </div>
                <p className="my-2 text-center sm:text-left">
                  The sizing information displayed are approximate measurements
                  of the actual garment itself and not body measurements.
                </p>
              </div>
              <div className="w-full max-w-[25rem] overflow-x-auto">
                <table className="table-fixed text-sm">
                  <tbody>
                    {REGION_SIZES.map((size, sizeIndex) => (
                      <tr key={sizeIndex}>
                        <td className="px-2 py-1 font-bold border-3 border-gray-200 bg-gray-900 text-white w-24">
                          {size.region}
                        </td>
                        {Array(5)
                          .fill(null)
                          .map((_, cellIndex) => (
                            <td
                              key={cellIndex}
                              className="bg-white font-bold border-3 border-gray-200 text-center px-2 py-1"
                            >
                              <span>{size.startSize + 4 * cellIndex}</span>
                              <span>&nbsp;-&nbsp;</span>
                              <span>{size.startSize + 4 * cellIndex + 2}</span>
                            </td>
                          ))}
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
              <div className="overflow-x-auto w-full max-w-[25rem]">
                <SizeTable
                  data={CENTIMETER_SIZES}
                  unit={sizeUnit}
                  className="mt-4"
                />
              </div>
              <div className="my-2 text-center sm:text-left">
                {CENTIMETER_SIZES.filter(size => size.note).map(
                  (size, index) => (
                    <div key={index}>
                      <span>
                        {Array(index + 1)
                          .fill('*')
                          .join('')}
                      </span>
                      <span>{size.note}</span>
                    </div>
                  ),
                )}
              </div>
              <div className="text-sm tracking-wider text-center sm:text-left">
                <span>MEASUREMENTS ARE SHOWN IN&nbsp;</span>
                <span className="font-bold">
                  {sizeUnit === 'cm' ? 'CM' : 'INCHES'}
                </span>
              </div>
              <div className="flex justify-center sm:justify-start">
                <Button
                  title={`SHOW IN ${sizeUnit === 'cm' ? 'CM' : 'INCHES'}`}
                  size="sm"
                  rounded
                  className="mt-4"
                  onClick={() => setSizeUnit(sizeUnit === 'cm' ? 'inch' : 'cm')}
                />
              </div>
            </div>
          </div>
          <div
            className={clsx(
              { hidden: selectedTab !== 'materials' },
              'flex flex-col items-center',
            )}
          >
            {productInfo.materials.map((material, index) => (
              <div key={index} className="flex w-96">
                <div className="flex-1 text-right font-bold text-2xl">
                  <span>{material.percent}</span>
                  <span>%</span>
                </div>
                <div className="flex-1 ml-3 flex items-center font-dosis">
                  {material.description}
                </div>
              </div>
            ))}
          </div>
          <div
            className={clsx(
              { hidden: selectedTab !== 'care' },
              'flex flex-col',
            )}
          >
            <div className="w-96 m-auto text-sm">
              {productInfo.care.map((care, index) => (
                <div key={index} className="flex">
                  <div className="w-[55px] h-[55px] flex-shrink-0">
                    <Image
                      src={care.imageUrl}
                      alt={care.imageUrl}
                      width="100%"
                      height="100%"
                    />
                  </div>
                  <div className="flex flex-auto items-center pl-4">
                    {care.description}
                  </div>
                </div>
              ))}
            </div>
            <div className="text-xs text-center mt-8">
              PLEASE NOTE: This item is sent in the manufacturer is original
              packaging and may require a light steam iron to remove any
              creases.
            </div>
          </div>
        </div>
      </div>
      <div className="font-bold text-2xl text-center mt-4 mb-8">
        YOU MAY ALSO LIKE
      </div>
      <RecommendProductSlider products={recommendProducts} />
      <div
        ref={reviewHeaderRef}
        className="font-bold text-2xl text-center mt-16 mb-8"
      >
        REVIEWS
      </div>
      <div className="px-4 sm:px-20 md:px-40 lg:px-60">
        <div className="flex sm:items-center flex-col sm:flex-row">
          <Rating rating={averageRating} />
          <div className="mt-1 sm:mt-0 sm:ml-3 flex items-center">
            <span className="font-bold text-2xl">{averageRating}</span>
            <span className="ml-3 text-gray-500">/&nbsp;5</span>
            <FontAwesomeIcon icon={faCircle} className="text-[5px] mx-3" />
            <span className="font-bold">{totalReview}</span>
            <span>&nbsp;review{totalReview > 1 && 's'}</span>
          </div>
        </div>
        <div className="w-full h-1 border-b border-gray-400 my-4" />
        <div className="text-xl mb-4">
          <span className="font-bold">Reviews</span>
          <span className="text-gray-500">&nbsp;{totalReview}</span>
        </div>
        <div className="flex flex-col gap-1">
          {RATING_LEVELS.map(level => (
            <ReviewProgressBar
              key={level.star}
              title={level.title}
              percentage={
                totalReview
                  ? Math.round(
                      (100 *
                        (reviewCounts.find(review => review._id === level.star)
                          ?.count || 0)) /
                        totalReview,
                    )
                  : 0
              }
            />
          ))}
        </div>
        <div className="mt-4">
          {reviews.map((review, index) => (
            <ReviewComment key={index} review={review} />
          ))}
          {!fetchingReview && currentPage < totalPage && (
            <div className="flex justify-center items-center h-10">
              <button
                onClick={() =>
                  dispatchGetReviewList(
                    productInfo._id,
                    currentPage + 1,
                    LIMIT_REVIEWS,
                  )
                }
                className="text-sm"
              >
                <span>Load more&nbsp;</span>
                <FontAwesomeIcon icon={faChevronDown} />
              </button>
            </div>
          )}
          {fetchingReview && (
            <div className="flex justify-center items-center h-10">
              <FontAwesomeIcon icon={faSpinner} className="animate-spin" />
            </div>
          )}
        </div>
        {grantedReview && <ReviewForm productId={productInfo._id} />}
      </div>
      <ImageViewer
        images={productImages}
        isOpening={isOpeningImageViewer}
        openingImageIndex={openingImageIndex}
        onClose={() => setIsOpeningImageViewer(false)}
        onClickPrev={handleClickPrev}
        onClickNext={handleClickNext}
      />
    </div>
  );
};

export default observer(ProductPage);

export const getServerSideProps: GetServerSideProps = async context => {
  const { product_name, id } = context.query;

  if (typeof product_name === 'string') {
    try {
      const result =
        typeof id === 'string'
          ? await fetchProductDetailById(id)
          : await fetchProductDetailByPath(product_name);

      return {
        props: {
          productInfo: result,
        },
      };
    } catch (err) {
      console.error(err);

      return {
        notFound: true,
      };
    }
  }

  return {
    props: {},
  };
};
