import { useEffect, useRef, useState } from 'react';
import { NextPage } from 'next';
import {
  faBoxesStacked,
  faCartShopping,
  faChartColumn,
  faChevronDown,
  faCube,
  faDashboard,
  faMoneyBill,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ChartCard from '../../components/ChartCard';
import { Line } from 'react-chartjs-2';
import { DateRangePicker, RangeKeyDict } from 'react-date-range';
import colors from 'tailwindcss/colors';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import Button from '../../components/Button';
import clsx from 'clsx';
import {
  endOfDay,
  format,
  isSameDay,
  isToday,
  isYesterday,
  startOfDay,
} from 'date-fns';
import {
  getShortDateString,
  isInLastMonth,
  isInLastWeek,
  isInThisMonth,
  isInThisWeek,
} from '../../utils/date';
import ManageCard from '../../components/ManageCard';
import Table from '../../components/Table';
import TableCard from '../../components/TableCard';
import ReportStore from '../../stores/ReportStore';
import { FilterTimeType } from '../../services/ReportService';
import { observer } from 'mobx-react-lite';
import UserStore from '../../stores/UserStore';
import { getRoundedDecimal } from '../../utils/math';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
);

const Dashboard: NextPage = () => {
  const [showDatePicker, setShowDatePicker] = useState<boolean>(false);
  const [dateText, setDateText] = useState<string>('Today');
  const [startDate, setStartDate] = useState<Date>(startOfDay(new Date()));
  const [endDate, setEndDate] = useState<Date>(endOfDay(new Date()));

  const { currencyExchangeRate, selectedCurrency } = UserStore;
  const {
    revenues,
    orders,
    newestOrders,
    revenueTotal,
    orderTotal,
    changePercentRevenue,
    changePercentOrder,
    changeTotalRevenue,
    changeTotalOrder,
    labels,
    reportOrders,
    topProducts,
    fetchingReportData,
    fetchingOrderData,
    fetchingTopProducts,
    fetchingNewestOrders,
    dispatchFetchReportDataByTime,
    dispatchFetchOrderDataByTime,
    dispatchFetchTopProductsByTime,
    dispatchFetchNewestOrders,
  } = ReportStore;

  const datePickerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    dispatchFetchNewestOrders();
  }, []);

  useEffect(() => {
    let filterType: FilterTimeType = 'day';

    if (isToday(startDate) && isToday(endDate)) {
      filterType = 'hour';
      setDateText('Today');
    } else if (isYesterday(startDate) && isYesterday(endDate)) {
      filterType = 'hour';
      setDateText('Yesterday');
    } else if (isInThisWeek(startDate, endDate)) {
      setDateText('This week');
    } else if (isInLastWeek(startDate, endDate)) {
      setDateText('Last week');
    } else if (isInThisMonth(startDate, endDate)) {
      setDateText('This month');
    } else if (isInLastMonth(startDate, endDate)) {
      setDateText('Last month');
    } else if (isSameDay(startDate, endDate)) {
      filterType = 'hour';
      setDateText(getShortDateString(startDate));
    } else {
      setDateText(
        `${getShortDateString(startDate)} -> ${getShortDateString(endDate)}`,
      );
    }

    const filterDate = {
      from: startDate.getTime(),
      to: endDate.getTime(),
      type: filterType,
    };

    dispatchFetchReportDataByTime(filterDate);
    dispatchFetchOrderDataByTime(filterDate);
    dispatchFetchTopProductsByTime(filterDate);
  }, [startDate, endDate]);

  useEffect(() => {
    const handleClickOutside = (e: any) => {
      datePickerRef.current &&
        !datePickerRef.current.contains(e.target) &&
        setShowDatePicker(false);
    };

    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [datePickerRef]);

  const handleChangeDate = (ranges: RangeKeyDict) => {
    const { startDate, endDate } = ranges.selection;

    if (startDate) {
      setStartDate(startOfDay(startDate));
    }
    if (endDate) {
      setEndDate(endOfDay(endDate));
    }
  };

  return (
    <div className="pb-4">
      <div className="flex justify-between items-center py-8">
        <div>
          <FontAwesomeIcon icon={faDashboard} />
          <span className="ml-2 font-semibold">Dashboard</span>
        </div>
        <div className="relative">
          <div
            ref={datePickerRef}
            className="absolute top-8 right-0 rounded-md w-date-picker md:w-auto overflow-auto"
          >
            <DateRangePicker
              className={clsx({ '!hidden': !showDatePicker })}
              ranges={[
                {
                  startDate,
                  endDate,
                  key: 'selection',
                },
              ]}
              onChange={handleChangeDate}
            />
          </div>
          <Button
            themeColor="blue"
            roundSize="md"
            size="sm"
            onClick={() => setShowDatePicker(!showDatePicker)}
          >
            <span>
              {dateText}&nbsp;
              <FontAwesomeIcon icon={faChevronDown} />
            </span>
          </Button>
        </div>
      </div>
      <div className="flex flex-col lg:flex-row gap-4">
        <div className="w-full lg:w-2/3">
          <div className="rounded-md shadow-md bg-white py-4">
            <div className="flex justify-center px-4">
              <ChartCard
                icon={faChartColumn}
                title="Revenue"
                total={getRoundedDecimal(
                  currencyExchangeRate * revenueTotal,
                  2,
                )}
                changePercent={Math.abs(changePercentRevenue)}
                quantity={getRoundedDecimal(
                  Math.abs(currencyExchangeRate * changeTotalRevenue),
                  2,
                )}
                unit={selectedCurrency.currencyText}
                increase={changeTotalRevenue > 0}
                loading={fetchingReportData}
                className="bg-green-500"
              />
              <ChartCard
                icon={faCartShopping}
                title="Order"
                total={orderTotal}
                changePercent={Math.abs(changePercentOrder)}
                quantity={Math.abs(changeTotalOrder)}
                unit="orders"
                loading={fetchingReportData}
                increase={changeTotalOrder > 0}
                className="bg-blue-400 ml-10"
              />
            </div>
            <div className="my-4">
              <Line
                data={{
                  labels,
                  datasets: [
                    {
                      data: revenues,
                      label: 'Revenue',
                      borderColor: colors.green[500],
                      yAxisID: 'y',
                    },
                    {
                      data: orders,
                      label: 'Order',
                      borderColor: colors.blue[500],
                      yAxisID: 'y1',
                    },
                  ],
                }}
                options={{
                  responsive: true,
                  interaction: {
                    mode: 'index',
                    intersect: false,
                  },
                  scales: {
                    x: {
                      title: {
                        display: true,
                        text: 'Timeline',
                      },
                    },
                    y: {
                      beginAtZero: true,
                      title: {
                        display: true,
                        text: 'Money',
                      },
                      type: 'linear',
                      display: true,
                      position: 'left',
                    },
                    y1: {
                      beginAtZero: true,
                      title: {
                        display: true,
                        text: 'Quantity',
                      },
                      type: 'linear',
                      display: true,
                      position: 'right',
                    },
                  },
                  plugins: {
                    legend: {
                      display: false,
                    },
                  },
                }}
              />
            </div>
          </div>
          <div className="flex flex-col md:flex-row gap-4 my-4">
            <TableCard
              headers={[
                {
                  field: 'order',
                  title: 'Order',
                  icon: faCartShopping,
                },
                {
                  field: 'quantity',
                  title: 'Quantity',
                },
                {
                  field: 'revenue',
                  title: 'Revenue',
                },
              ]}
              data={reportOrders.map(reportOrder => ({
                order: reportOrder.label,
                quantity: reportOrder.count,
                revenue:
                  selectedCurrency.currencyUnit +
                  getRoundedDecimal(
                    currencyExchangeRate * reportOrder.revenueTotal,
                    2,
                  ),
              }))}
              loading={fetchingOrderData}
              className="flex-1"
            />
            <TableCard
              headers={[
                {
                  field: 'name',
                  title: 'Top product',
                  icon: faCube,
                },
                {
                  field: 'quantity',
                  title: 'Quantity',
                },
                {
                  field: 'stock',
                  title: 'Stock',
                },
                {
                  field: 'revenue',
                  title: 'Revenue',
                },
              ]}
              data={topProducts.map(product => ({
                name: (
                  <span className="text-xs">{`${product.parent_info?.[0]?.name} - ${product.colour}`}</span>
                ),
                quantity: product.quantityTotal,
                stock: product.stockTotal,
                revenue:
                  selectedCurrency.currencyUnit +
                  getRoundedDecimal(product.revenueTotal, 2),
              }))}
              loading={fetchingTopProducts}
              className="flex-1"
            />
          </div>
        </div>
        <ManageCard
          icon={faMoneyBill}
          title="New orders"
          isEmpty={!newestOrders.length}
          className="w-full lg:w-1/3"
        >
          <Table
            showNumber
            headers={[
              {
                field: 'email',
                title: 'Customer email',
              },
              {
                field: 'price',
                title: 'Total price	',
              },
              {
                field: 'date',
                title: 'Order date',
              },
            ]}
            data={newestOrders.map(order => ({
              email: order.customerEmail,
              price:
                selectedCurrency.currencyUnit +
                getRoundedDecimal(currencyExchangeRate * order.totalAmount, 2),
              date: format(new Date(order.confirmDate), 'dd/MM/yyyy, HH:mm'),
            }))}
            isLoading={fetchingNewestOrders}
          />
        </ManageCard>
      </div>
    </div>
  );
};

export default observer(Dashboard);
